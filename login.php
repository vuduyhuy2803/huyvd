<?php 
    session_start();
    if (isset($_POST['login'])) {
        $email = 'huyvd@gmail.com';
        $password = 'huyvd123';
        if ($_POST['email'] == $email && $_POST['password'] == $password) {
            $_SESSION['name'] = $email;
        } else {
            echo 'đăng nhập thất bại';
        }
        if (isset($_SESSION['name'])) {
            header('Location:./LoginSuccess.php');
        }
        if (isset($_POST['remember'])) {
            setcookie('email', $email, time() + 50, '/', '', 0, 0);
            setcookie('password', $password, time() + 50, '/', '', 0, 0);
        }
    }
 ?>
<!DOCTYPE html>
<html>
<head>
    <title>PHP web</title>
    <meta charset="utf-8">
</head>
<body>
<?php
    $error = array();
    $data = array();
    if (!empty($_POST['login'])) {
        $data['email'] = isset($_POST['email']) ? $_POST['email'] : '';
        $data['password'] = isset($_POST['password']) ? $_POST['password'] : '';
        require('./validate.php');
        if (empty($data['email'])) {
            $error['email'] = 'Bạn chưa nhập email';
        } elseif (!is_email($data['email'])) {
            $error['email'] = 'Email không đúng định dạng';
        }
        if (empty($data['password'])) {
            $error['password'] = 'Bạn chưa nhập mật khẩu';
        } elseif (!is_password($data['password'])) {
            $error['password'] = 'password không đúng định dạng';
        }
}
?>
    <form method="POST">
        <table>
            <tr>
                <td>Nhập email</td>
                <td>
                    <input type="text" name="email" id="email" value="<?php echo $data['email'] ?? ''; ?>"/>
                </td>
                <td style="color: red;">
                    <?php echo isset($error['email']) ? $error['email'] : ''; ?>                        
                </td>
            </tr>
            <tr>
                <td>Nhập mật khẩu</td>
                <td>
                    <input type="password" name="password" id="password" value="<?php echo $data['password'] ?? ''; ?>"/>
                </td>
                <td style="color: red;">
                    <?php echo isset($error['password']) ? $error['password'] : ''; ?>
                </td>
            </tr>
            <tr>
                <td>Remember me</td>
                <td>
                    <input type="checkbox" name="remember" value="" <?php if(isset($_COOKIE['email'])) echo "checked";?>>
                </td>
            </tr>
            <tr>
                <td></td>
                <td><input type="submit" name="login" value="Login"></td>
            </tr>
        </table>
    </form>
</body>
</html>