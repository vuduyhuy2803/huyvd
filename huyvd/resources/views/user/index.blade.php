<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Danh sách</title>
    <link href="{{ url('twbs/bootstrap/dist/css/bootstrap.min.css')}}" style="text/css" rel="stylesheet">
    <style type="text/css">
        .example{
            margin: 20px;
        }
    </style>
</head>
<body>
    <div class="example">
        <div class="container">
            <div class="row">
                <h2>Danh sách</h2>
                <table class="table table-bordered">
                    <thead>
                        <tr style="text-align: center;">
                            <th>STT</th>
                            <th>Tên</th>
                            <th>Địa chỉ email</th>
                            <th>Địa chỉ</th>
                            <th>Số điện thoại</th>
                        </tr>
                    </thead>
                    <tbody>
                        <<?php $stt=1; ?>
                        @foreach($user as $value)
                        <tr>
                            <td>{{$stt++}}</td>
                            <td>{{$value->name}}</td>
                            <td>{{$value->mail_address}}</td>
                            <td>{{$value->address}}</td>
                            <td>{{$value->phone}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <a href="{{route('users.create') }}" class="btn btn-lg btn-info">Thêm user</a>
            </div >
        </div>
    </div>
    {{ $user->links()}}
</body>
</html>