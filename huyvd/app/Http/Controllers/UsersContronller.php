<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CreateUserRequest;
use App\Models\User;

class UsersContronller extends Controller
{
    public function index()
    {
        $users = User::orderBy('mail_address', 'ASC')->paginate(20);
        return view('user.index',['user'=>$users]);
    }
    public function store(CreateUserRequest $Request)
    {
        $users = $Request->All();
        $users['password'] = bcrypt($Request->password);
        User::create($users);
        $Request->session()->flash('status', 'Thêm người dùng thành công!');
        return redirect()->route('users.create');
    }
    public function create()
    {
        return view('user.add');
    }
}
