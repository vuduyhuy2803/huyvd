<?php
abstract class Supervisor 
{
    protected $slogan;
    abstract function saySloganOutLoud();
}

interface Boss 
{
    public function checkValidSlogan();
}

class EasyBoss extends Supervisor implements Boss
{
    use Active;
    public function setSlogan($string) {
        $this->slogan = $string;
    }
    public function saySloganOutLoud() {
        return $this->slogan;
    }
    
    public function checkValidSlogan() {
        return (strpos($this->slogan, 'before') !== false || strpos($this->slogan, 'after') !== false);
        
    }
}

class UglyBoss extends Supervisor implements Boss
{
    use Active;
    public function setSlogan($string) {
        $this->slogan = $string;
    }
    public function saySloganOutLoud()
    {
        return $this->slogan;
    }

    public function checkValidSlogan() {
        return (strpos($this->slogan, 'before') !== false && strpos($this->slogan, 'after') !== false); 
    }
}
trait Active
{
    public function defindYourSelf() {
        return get_class($this);
    }
}
$easyBoss = new EasyBoss();
$uglyBoss = new UglyBoss();
$easyBoss->setSlogan('Do NOT push anything to master branch before reviewed by supervisor(s)');
$uglyBoss->setSlogan('Do NOT push anything to master branch before reviewed by supervisor(s). Only they can do it after check it all!');
$easyBoss->saySloganOutLoud(); 
echo "<br>";
$uglyBoss->saySloganOutLoud(); 

echo "<br>";

var_dump($easyBoss->checkValidSlogan());
echo "<br>";
var_dump($uglyBoss->checkValidSlogan());

echo "<br>";

echo 'I am ' . $easyBoss->defindYourSelf(); 
echo "<br>";
echo 'I am ' . $uglyBoss->defindYourSelf(); 
?>