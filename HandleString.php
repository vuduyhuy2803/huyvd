<?php
class handleString {
    public $check1, $check2;

    /**
    *
    * read file
    * @param $file $file
    * @return boolean
    */
    public function readFile($file) {
        if (file_exists($file)) {
            $openfile = @fopen($file, "r");
            if (!$openfile) {
                echo 'không mở được file';
            } else {
                return fread($openfile, filesize($file));
            }
        } else {
               echo 'file không tồn tại';
        }
    }
 

    /**
    *
    * check string
    * @param $string $string
    * @return boolean
    */   
    public function checkValidString ($string) {
        if (empty($string)) {
            return true;
        } elseif (strlen($string) > 50 && strpos($string, "after") === false) {
            return true;
        } elseif (!empty($string) && strpos($string, "after") === false && strpos($string, "before") !== false) {
            return true;
        } else {
            return false;
        }
    }
}
$object1 = new handleString();
$object1->check1 = var_dump($object1->checkValidString($object1->readFile('file1.txt')));
echo '<hr/>';
$object1->check2 = var_dump($object1->checkValidString($object1->readFile('file2.txt')));

?>