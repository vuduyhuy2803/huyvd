<?php 
    include_once './vendor/autoload.php';
    try {
    $conn = new PDO(
        'mysql:host=localhost;dbname=huyvd;charset=utf8',
        'root',
        ''
    );

    } catch (PDOException $ex) {
        echo 'Ket noi that bai';
    }
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="stylesheet" type="text/css" href="vendor/twbs/bootstrap/dist/css/bootstrap.min.css">
    <style type="text/css" media="screen">
        .container {
            margin: auto;
        }
        .form-signin {
            margin: auto;
        }
        .checkbox {
            margin-top: 5px;
        }
        .please-sign-in {
            text-align: center;
        }
    </style>
</head>
<body>
    <div class = "container">
        <?php
            $error = array();
            $data = array();
            if (!empty($_POST['login'])) {
                $email = $_POST['email'];
                $password = $_POST['password'];
                $passwordConfirm = $_POST['passwordConfirm'];
                $data['email'] = isset($_POST['email']) ? $_POST['email'] : '';
                $data['password'] = isset($_POST['password']) ? $_POST['password'] : '';
                $data['passwordConfirm'] = isset($_POST['passwordConfirm']) ? $_POST['passwordConfirm'] : '';

                // check password
                function is_password($password)
                {
                    return (preg_match("/[a-zA-Z0-9]{6,25}/", $password));
                }

                // check email
                function is_email($email)
                {
                    return (!filter_var($email, FILTER_VALIDATE_EMAIL)) ? FALSE : TRUE;
                }

                if (empty($data['email'])) {
                    $error['email'] = 'Bạn chưa nhập email';
                } elseif (!is_email($data['email'])) {
                    $error['email'] = 'Email không đúng định dạng';
                }
                if (empty($data['password'])) {
                    $error['password'] = 'Bạn chưa nhập mật khẩu';
                } elseif (!is_password($data['password'])) {
                    $error['password'] = 'password không đúng định dạng';
                } else if ($data['passwordConfirm'] != $data['password']) {
                    $error['password_confirm'] = 'mật khẩu không trùng khớp';
                }

                if (!isset($error['email']) && !isset($error['password']) && !isset($error['passwordConfirm'])) {
                    $query = "SELECT mail_address FROM users WHERE mail_address = :mail_address";
                    $stmt = $conn->prepare($query);
                    $stmt->bindValue(':mail_address', $data['email']);
                    $stmt->execute();
                    $user = $stmt->fetch(PDO::FETCH_ASSOC);
                    if ($user) {
                        $error['email'] = 'Email đã được sử dụng';
                    } else {
                        $created_at = strtotime("now");
                        $data['password'] = md5($data['password']);
                        $query = $conn->prepare('
                                INSERT INTO users (mail_address, password)
                                VALUES (:mail_address, :password)
                            ');
                        $query->bindParam(':mail_address', $data['email']);
                        $query->bindParam(':password', $data['password']);
                        $query->execute();
                        echo "Đăng ký thành công";
                        header("Location: LoginPdo.php");
                    }
                }
            }
        ?>


        <form class="form-signin col-md-4" method="post">
            <h1 class="h3 mb-3 font-weight-normal please-sign-in">Please sign in</h1>
            <label for="inputEmail">Email address</label>
            <input type="text" class="form-control" placeholder="Email address" name="email" >
            <div style="color: red;">
                <?php echo isset($error['email']) ? $error['email'] : ''; ?>
            </div>
            <label for="inputPassword">Password</label>
            <input type="password" class="form-control" placeholder="Password" name="password">
            <div style="color: red;">
                <?php echo isset($error['password']) ? $error['password'] : ''; ?>
            </div>
            <label for="inputPassword">Password Confirm</label>
            <input type="password" class="form-control" placeholder="Password Confirm" name="passwordConfirm" >
            <div style="color: red;">
                <?php echo isset($error['passwordConfirm']) ? $error['passwordConfirm'] : ''; ?>
            </div>
            <div class="checkbox mb-3">
                <label class="checkbox">
                    <input type="checkbox" value="remember-me"> Remember me
                </label>
            </div>
            <input class="btn btn-lg btn-primary btn-block" type="submit" name="login" value="Sign in">
        </form>
        <?php 

        ?>
    </div>
</body>
</html>